# Authors

Currently maintained by **Nico Brandt**
([nico.brandt@kit.edu](mailto:nico.brandt@kit.edu)).

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Nico Brandt**
* **Christoph Herrmann**
* **Ephraim Schoof**
* **Giovanna Tosato**
* **Lars Griem**
* **Philipp Zschumme**
* **Moritz Huck**
* **Johannes Steinhülb**
* **Luka Radosevic**
* **Manideep Jayavarapu**
