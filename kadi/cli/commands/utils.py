# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import subprocess
import sys

import click
from flask import current_app
from jinja2 import Template

from kadi.cli.main import kadi
from kadi.cli.utils import check_env
from kadi.cli.utils import echo
from kadi.cli.utils import echo_danger
from kadi.cli.utils import echo_success

# Neu
from sphinxcontrib.autohttp.flask_base import get_routes
import re
import kadi.lib.constants as const
from kadi.lib.schemas import CustomString
from werkzeug.http import HTTP_STATUS_CODES
from kadi.modules.records.schemas import RecordSchema

from marshmallow import fields, Schema
import json
from collections import OrderedDict


DEFAULT_USER = "kadi"
DEFAULT_GROUP = "www-data"
DEFAULT_CONFIG_FILE = "/opt/kadi/config/kadi.py"
DEFAULT_INI_FILE = "/etc/kadi-uwsgi.ini"
DEFAULT_CERT_FILE = "/etc/ssl/certs/kadi.crt"
DEFAULT_KEY_FILE = "/etc/ssl/private/kadi.key"


@kadi.group()
def utils():
    """Miscellaneous utility commands."""


@utils.command()
def config():
    """Print the current Kadi configuration."""
    for key, value in sorted(current_app.config.items()):
        echo(f"{key}: {value}")


@utils.command()
@click.option("-p", "--port", help="The port to bind to.", default=8_025)
@check_env
def smtpd(port):
    """Run a simple SMTP server for debugging purposes."""
    echo(f"Listening on localhost:{port}...")

    try:
        subprocess.run(["python", "-m", "aiosmtpd", "-n", "-l", f"localhost:{port}"])
    except KeyboardInterrupt:
        pass


def _generate_config(template_name, outfile=None, **kwargs):
    template_path = os.path.join(
        current_app.root_path, "cli", "templates", template_name
    )

    with open(template_path, encoding="utf-8") as f:
        template = Template(f.read())

    rendered_template = template.render(**kwargs)

    if outfile is not None:
        if os.path.exists(outfile.name):
            echo_danger(f"'{outfile.name}' already exists.")
            sys.exit(1)

        outfile.write(f"{rendered_template}\n")
        echo_success(f"File '{outfile.name}' generated successfully.")
    else:
        echo(f"\n{rendered_template}", bold=True)


@utils.command()
@click.option(
    "-d", "--default", is_flag=True, help="Use the default values for all prompts."
)
@click.option(
    "-o", "--out", type=click.File(mode="w"), help="Output file (e.g. kadi.conf)."
)
def apache(default, out):
    """Generate a basic Apache web server configuration."""
    DEFAULT_CHAIN_FILE = ""

    if default:
        cert_file = DEFAULT_CERT_FILE
        key_file = DEFAULT_KEY_FILE
        chain_file = DEFAULT_CHAIN_FILE
    else:
        cert_file = click.prompt("SSL/TLS certificate file", default=DEFAULT_CERT_FILE)
        key_file = click.prompt("SSL/TLS key file", default=DEFAULT_KEY_FILE)
        chain_file = click.prompt(
            "SSL/TLS intermediate certificates chain file (optional)",
            default=DEFAULT_CHAIN_FILE,
        )

    anonip_bin = None

    if click.confirm(
        "Anonymize IP addresses in access logs using the 'anonip' Python package?",
        default=True,
    ):
        anonip_bin = os.path.join(sys.prefix, "bin", "anonip")

    _generate_config(
        "kadi.conf",
        outfile=out,
        server_name=current_app.config["SERVER_NAME"],
        storage_path=current_app.config["STORAGE_PATH"],
        misc_uploads_path=current_app.config["MISC_UPLOADS_PATH"],
        static_path=current_app.static_folder,
        cert_file=cert_file,
        key_file=key_file,
        chain_file=chain_file,
        anonip_bin=anonip_bin,
    )


@utils.command()
@click.option(
    "-d", "--default", is_flag=True, help="Use the default values for all prompts."
)
@click.option(
    "-o", "--out", type=click.File(mode="w"), help="Output file (e.g. kadi.ini)."
)
def uwsgi(default, out):
    """Generate a basic uWSGI application server configuration."""
    if default:
        uid = DEFAULT_USER
        gid = DEFAULT_GROUP
        kadi_config = DEFAULT_CONFIG_FILE
    else:
        uid = click.prompt("User the server will run under", default=DEFAULT_USER)
        gid = click.prompt("Group the server will run under", default=DEFAULT_GROUP)
        kadi_config = click.prompt(
            "Kadi configuration file", default=DEFAULT_CONFIG_FILE
        )

    _generate_config(
        "kadi-uwsgi.ini",
        outfile=out,
        num_processes=min(os.cpu_count() or 4, 50),
        root_path=current_app.root_path,
        venv_path=sys.prefix,
        uid=uid,
        gid=gid,
        kadi_config=kadi_config,
    )


@utils.command()
@click.option(
    "-d", "--default", is_flag=True, help="Use the default values for all prompts."
)
@click.option(
    "-o",
    "--out",
    type=click.File(mode="w"),
    help="Output file (e.g. kadi-uwsgi.service).",
)
def uwsgi_service(default, out):
    """Generate a basic systemd unit file for uWSGI."""
    if default:
        uid = DEFAULT_USER
        gid = DEFAULT_GROUP
        kadi_ini = DEFAULT_INI_FILE
    else:
        uid = click.prompt("User the service will run under", default=DEFAULT_USER)
        gid = click.prompt("Group the service will run under", default=DEFAULT_GROUP)
        kadi_ini = click.prompt("uWSGI configuration file", default=DEFAULT_INI_FILE)

    _generate_config(
        "kadi-uwsgi.service",
        outfile=out,
        uwsgi_bin=os.path.join(sys.prefix, "bin", "uwsgi"),
        uid=uid,
        gid=gid,
        kadi_ini=kadi_ini,
    )


@utils.command()
@click.option(
    "-d", "--default", is_flag=True, help="Use the default values for all prompts."
)
@click.option(
    "-o",
    "--out",
    type=click.File(mode="w"),
    help="Output file (e.g. kadi-celery.service).",
)
def celery(default, out):
    """Generate a basic systemd unit file for Celery."""
    if default:
        uid = DEFAULT_USER
        gid = DEFAULT_GROUP
        kadi_config = DEFAULT_CONFIG_FILE
    else:
        uid = click.prompt("User the service will run under", default=DEFAULT_USER)
        gid = click.prompt("Group the service will run under", default=DEFAULT_GROUP)
        kadi_config = click.prompt(
            "Kadi configuration file", default=DEFAULT_CONFIG_FILE
        )

    _generate_config(
        "kadi-celery.service",
        outfile=out,
        kadi_bin=os.path.join(sys.prefix, "bin", "kadi"),
        uid=uid,
        gid=gid,
        kadi_config=kadi_config,
    )


@utils.command()
@click.option(
    "-d", "--default", is_flag=True, help="Use the default values for all prompts."
)
@click.option(
    "-o",
    "--out",
    type=click.File(mode="w"),
    help="Output file (e.g. kadi-celerybeat.service).",
)
def celerybeat(default, out):
    """Generate a basic systemd unit file for Celery Beat."""
    if default:
        uid = DEFAULT_USER
        gid = DEFAULT_GROUP
        kadi_config = DEFAULT_CONFIG_FILE
    else:
        uid = click.prompt("User the service will run under", default=DEFAULT_USER)
        gid = click.prompt("Group the service will run under", default=DEFAULT_GROUP)
        kadi_config = click.prompt(
            "Kadi configuration file", default=DEFAULT_CONFIG_FILE
        )

    _generate_config(
        "kadi-celerybeat.service",
        outfile=out,
        kadi_bin=os.path.join(sys.prefix, "bin", "kadi"),
        uid=uid,
        gid=gid,
        kadi_config=kadi_config,
    )

MISSING = "MISSING"

@utils.command()
def apispec():
    def filter_routes(app):
        openapi_spec = initialize_openapi_spec()
        routes = get_routes(app, order="path")

        for method, paths, endpoint in routes:
            process_route(app, method, paths[0], endpoint, openapi_spec)
        save_openapi_spec(openapi_spec)

    filter_routes(current_app)


def initialize_openapi_spec():
    return {
        "openapi": "3.0.0",
        "info": {
            "title": "Records API",
            "version": "1.0.0",
            "description": (
                "To get more detailed information about the contents of the requested/returned "
                "representation of resources related to records, see also [Record](https://kadi4mat.readthedocs.io/en/stable/apiref/modules.html#kadi.modules.records.models.Record), "
                "especially [Record.extras](https://kadi4mat.readthedocs.io/en/stable/apiref/modules.html#kadi.modules.records.models.Record.extras), "
                "[RecordLink](https://kadi4mat.readthedocs.io/en/stable/apiref/modules.html#kadi.modules.records.models.RecordLink), "
                "[File](https://kadi4mat.readthedocs.io/en/stable/apiref/modules.html#kadi.modules.records.models.File), "
                "[Upload](https://kadi4mat.readthedocs.io/en/stable/apiref/modules.html#kadi.modules.records.models.Upload), "
                "and [Revision](https://kadi4mat.readthedocs.io/en/stable/apiref/lib.html#kadi.lib.revisions.models.Revision). "
                "Note that for tags ([Tag](https://kadi4mat.readthedocs.io/en/stable/apiref/lib.html#kadi.lib.tags.models.Tag)) and licenses "
                "([License](https://kadi4mat.readthedocs.io/en/stable/apiref/lib.html#kadi.lib.licenses.models.License)), only "
                "their names are relevant for the API. For possible license and role values, see also the [Miscellaneous](https://kadi4mat.readthedocs.io/en/stable/httpapi/latest/misc.html#httpapi-latest-misc) endpoints.\n\n"
            ),
        },
        "paths": OrderedDict(),
    }


def process_route(app, method, path, endpoint, openapi_spec):
    view = app.view_functions[endpoint]
    view_func = getattr(view, "view_class", None) or view

    view_doc = view.__doc__ or view_func.__doc__ or ""
    if not view_doc:
        return

    apidoc_meta = getattr(view_func, const.APIDOC_META_ATTR, {})
    reqschema_meta = apidoc_meta.get(const.APIDOC_REQ_SCHEMA_KEY, {})
    pagination_meta = apidoc_meta.get(const.APIDOC_PAGINATION_KEY, {})
    status_codes_meta = apidoc_meta.get(const.APIDOC_STATUS_CODES_KEY, {})
    scopes = apidoc_meta.get(const.APIDOC_SCOPES_KEY)

    
    add_path_to_spec(path, openapi_spec)

    operation_spec = build_operation_spec(method, view_doc, endpoint, pagination_meta, apidoc_meta, scopes)

    # Füge alle Path-Parameter hinzu, die im Path vorhanden sind (z.B. {id})
    path_parameters = extract_path_parameters(path)
    
    operation_spec["parameters"].extend(path_parameters)

    if method.lower() in ["post", "patch"]:
        add_request_body(operation_spec, reqschema_meta)

    add_status_codes(operation_spec, status_codes_meta)

    openapi_spec["paths"][path][method.lower()] = operation_spec
    


def extract_path_parameters(path):
    """Extrahiere Path-Parameter aus dem gegebenen Pfad."""
    parameters = []
    path_parts = path.split("/")
    for part in path_parts:
        if part.startswith("{") and part.endswith("}"):
            param_name = part[1:-1]
            parameters.append({
                "name": param_name,
                "in": "path",
                "required": True,
                "schema": {"type": "string"},
                "description": f"Path parameter `{param_name}`",
            })
    return parameters


def format_path(path):
    """Formatiere den Pfad, um Parameter wie (int:id) in {id} umzuwandeln."""
    # Passe hier das Mapping an, falls du mehr Parametertypen verwendest
    formatted_path = path.replace("(int:id)", "{id}").replace("(export_type)", "{export_type}")
    return formatted_path





def add_path_to_spec(path, openapi_spec):
    if path not in openapi_spec["paths"]:
        openapi_spec["paths"][path] = OrderedDict()


def build_operation_spec(method, view_doc, endpoint, pagination_meta, apidoc_meta, scopes):
    parameters = build_parameters(pagination_meta, apidoc_meta)

    return {
        "description": view_doc,
        "operationId": endpoint,
        "scope": scopes,
        "parameters": parameters,
        "responses": {},
    }


def build_parameters(pagination_meta, apidoc_meta):
    parameters = []
    if pagination_meta:
        add_pagination_parameters(parameters, pagination_meta)

    for param_name, param_meta in apidoc_meta.get(const.APIDOC_QPARAMS_KEY, {}).items():
        param_info = {
            "name": param_name,
            "in": "query",
            "required": False,
            "schema": {"type": "string"},
            "description": param_meta.get("description", ""),
        }
        parameters.append(param_info)

    return parameters


def add_pagination_parameters(parameters, pagination_meta):
    if "per_page_max" in pagination_meta:
        max_per_page = pagination_meta["per_page_max"]

        parameters.append({
            "name": "page",
            "in": "query",
            "required": False,
            "schema": {"type": "integer"},
            "description": "The page number to receive. (*Default:* 1)",
        })
        parameters.append({
            "name": "per_page",
            "in": "query",
            "required": False,
            "schema": {"type": "integer", "maximum": max_per_page},
            "description": f"The number of records per page. Maximum is {max_per_page}. (*Default:* 10)",
        })


def add_request_body(operation_spec, reqschema_meta):
    schema = reqschema_meta.get("schema")
    if schema:
        description = reqschema_meta.get("description")
        fields = _get_reqschema_fields(schema)
        required_fields = [field for field, info in fields.items() if info["required"]]
        
        
        requestBody = {
            "content": {
                "application/json": {
                    "schema": {
                        "type": "object",
                        "properties": {},
                        "required": required_fields,
                        "description": description,
                    }
                }
            }
        }

        for field_name, field_info in fields.items():
            field_type = field_info["type"].lower()
            print(f"{field_name }:{field_info}")
            if field_info["many"]:
                print(f"{field_name }:{field_info}")
                requestBody["content"]["application/json"]["schema"]["properties"][field_name] = {
                    "type": "array",
                    "items": {"type": field_type},
                }
            else:
                requestBody["content"]["application/json"]["schema"]["properties"][field_name] = {
                    "type": field_type
                }

        if required_fields:
            operation_spec["requestBody"] = {
                "required": True,
                **requestBody,
            }


def add_status_codes(operation_spec, status_codes_meta):
    if not status_codes_meta:
        operation_spec["responses"][200] = {"description": "return"}
    else:
        for status_code, data in status_codes_meta.items():
            description = data.get("description", "")
            schema_instance = data.get("schema", None)

            if schema_instance is not None:
                fields = _get_reqschema_fields(schema_instance)
                required_fields = [field for field, info in fields.items() if info["required"]]

                response_schema = {
                    "type": "object",
                    "properties": {},
                    "required": required_fields,
                }

                for field_name, field_info in fields.items():
                    field_type = field_info["type"].lower()
                    if field_info["many"]:
                        response_schema["properties"][field_name] = {
                            "type": "array",
                            "items": {"type": field_type},
                        }
                    else:
                        response_schema["properties"][field_name] = {"type": field_type}

                operation_spec["responses"][status_code] = {
                    "description": description,
                    "content": {
                        "application/json": {"schema": response_schema}
                    },
                }
            else:
                operation_spec["responses"][status_code] = {
                    "description": description
                }


def save_openapi_spec(openapi_spec):
    openapi_json = json.dumps(openapi_spec, indent=2)
    json_file_path = "/home/dominik/workspace/kadi/docs/source/openapi_spec.json"

    with open(json_file_path, "w") as json_file:
        json_file.write(openapi_json)

    click.echo(f"OpenAPI JSON-Spezifikation wurde in {json_file_path} gespeichert.")


SCHEMA_TYPE_MAPPING = {
    CustomString: "string",
    fields.String: "string",
    fields.Integer: "integer",
    fields.Float: "number",
    fields.Boolean: "boolean",
    fields.DateTime: "string",
    fields.List: "array",
    fields.Dict: "object",
}

SCHEMA_TYPE_FALLBACK = "string"


def _get_reqschema_fields(schema, is_partial=False):
    fields_meta = {}

    for name, field in schema.fields.items():
        if field.dump_only:
            continue

        field_meta = {
            "required": _get_field_attr(field, "required", False),
            "many": _get_field_attr(field, "many", False),
            "type": _get_field_attr(
                field,
                "type",
                SCHEMA_TYPE_MAPPING.get(field.__class__, SCHEMA_TYPE_FALLBACK),
            ),
        }

        is_partial = is_partial or (
            schema.partial is True
            or (isinstance(schema.partial, tuple) and name in schema.partial)
        )

        if is_partial:
            field_meta["required"] = False

        if isinstance(field, fields.Pluck):
            field_meta["type"] = SCHEMA_TYPE_MAPPING.get(
                field.schema.fields[field.field_name].__class__,
                SCHEMA_TYPE_FALLBACK,
            )
        elif isinstance(field, fields.Nested):
            # Pass along whether the schema is loaded partially, as we can't retrieve
            # this info from the nested schema directly.
            field_meta["nested"] = _get_reqschema_fields(
                field.schema, is_partial=is_partial
            )

        fields_meta[name] = field_meta

    sorted_fields = sorted(fields_meta.items(), key=lambda field: field[0])
    sorted_fields = sorted(
        sorted_fields, key=lambda field: field[1]["required"], reverse=True
    )

    return dict(sorted_fields)


def _get_field_attr(field, attr, default=None):
    # Try to retrieve the attribute from the custom field metadata first.
    if attr in field.metadata:
        return field.metadata[attr]

    if hasattr(field, attr):
        return getattr(field, attr)

    return default
    
