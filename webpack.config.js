/* Copyright 2020 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import path from 'path';
import url from 'url';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import VueLoaderPlugin from 'vue-loader/lib/plugin.js';
import {globSync} from 'glob';

const cssLoader = {
  loader: 'css-loader',
  options: {
    esModule: false,
  },
};

const currentDir = path.dirname(url.fileURLToPath(import.meta.url));
const scriptsDir = 'kadi/assets/scripts/';
const entryPoints = {main: `${scriptsDir}main.js`};

globSync(`${scriptsDir}app/**/*.js`).forEach((fileName) => {
  entryPoints[fileName.substring(scriptsDir.length, fileName.length - 3)] = fileName;
});

export default (env, argv) => {
  return {
    devtool: argv.mode === 'development' ? 'eval-source-map' : false,
    entry: entryPoints,
    optimization: {
      splitChunks: {
        chunks() {
          return false;
        },
      },
    },
    output: {
      chunkFilename: 'chunks/[name]-[contenthash].js',
      clean: true,
      devtoolFallbackModuleFilenameTemplate: 'webpack:///[resource-path]?[hash]',
      devtoolModuleFilenameTemplate: (info) => {
        return info.resourcePath.match(/\.vue$/) && info.allLoaders
          ? `webpack-generated:///${info.resourcePath}?${info.hash}`
          : `webpack:///${path.normalize(info.resourcePath)}`;
      },
      hashDigestLength: 32,
      hashFunction: 'md5',
      path: path.join(currentDir, 'kadi/static/dist'),
    },
    module: {
      rules: [
        {
          test: /\.m?js$/,
          resolve: {
            fullySpecified: false,
          },
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.css$/,
          use: [
            'vue-style-loader',
            cssLoader,
            'postcss-loader',
          ],
        },
        {
          test: /\.scss$/,
          use: [
            'vue-style-loader',
            cssLoader,
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /\/styles\/main\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            cssLoader,
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /fa-.*\.(ttf|woff2)$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name]-v6.6.0[ext]',
          },
        },
        {
          test: /lato-.*\.woff2?$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name]-v3.0.0[ext]',
          },
        },
        {
          test: /KaTeX_.*\.(ttf|woff2?)$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name]-v0.16.11[ext]',
          },
        },
      ],
    },
    performance: {
      hints: false,
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
      new VueLoaderPlugin(),
    ],
    resolve: {
      alias: {
        vue: 'vue/dist/vue.esm.js',
      },
      modules: ['.', './node_modules'],
    },
  };
};
