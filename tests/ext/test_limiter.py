# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app
from limits import parse_many

from kadi.ext.limiter import limiter
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


def _exhaust_rate_limit(
    client, endpoint, limit_per_sec, is_api_response=False, is_limited=True
):
    limiter.reset()

    for i in range(limit_per_sec + 1):
        response = client.get(url_for(endpoint))

        if i < limit_per_sec or not is_limited:
            assert response.status_code != 429
        else:
            if is_api_response:
                check_api_response(response, status_code=429)
            else:
                check_view_response(response, status_code=429)

            assert "Retry-After" in response.headers
            assert int(response.headers["Retry-After"]) >= 1

        # Make sure that static endpoints are always excluded.
        response = client.get(url_for("static", filename="favicons/favicon.ico"))
        response.close()

        assert response.status_code != 429


@pytest.mark.parametrize("is_whitelisted", [True, False])
def test_default_limit(is_whitelisted, monkeypatch, client):
    """Test the default rate limit per second for anonymous users."""
    if not is_whitelisted:
        monkeypatch.setitem(current_app.config, "RATELIMIT_IP_WHITELIST", [])

    for limit in parse_many(current_app.config["RATELIMIT_ANONYMOUS_USER"]):
        if limit.GRANULARITY.name == "second":
            _exhaust_rate_limit(
                client,
                "main.index",
                limit.amount,
                is_limited=not is_whitelisted,
            )
            _exhaust_rate_limit(
                client,
                "api.index",
                limit.amount,
                is_api_response=True,
                is_limited=not is_whitelisted,
            )
