# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from urllib.parse import parse_qs
from urllib.parse import urlparse

import pytest
from authlib.oauth2.rfc7636 import create_s256_code_challenge
from flask import current_app

import kadi.lib.constants as const
from kadi.lib.config.core import set_sys_config
from kadi.lib.oauth.models import OAuth2ServerAuthCode
from kadi.lib.oauth.models import OAuth2ServerClient
from kadi.lib.oauth.models import OAuth2ServerToken
from kadi.lib.security import random_alnum
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_index(client, user_session):
    """Test the "main.index" endpoint."""
    endpoint = url_for("main.index")

    response = client.get(endpoint)
    check_view_response(response)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)


def test_about(client):
    """Test the "main.about" endpoint."""
    response = client.get(url_for("main.about"))
    check_view_response(response)


def test_help(client):
    """Test the "main.help" endpoint."""
    response = client.get(url_for("main.help"))
    check_view_response(response)


@pytest.mark.parametrize(
    "endpoint,config_item",
    [
        ("terms_of_use", const.SYS_CONFIG_TERMS_OF_USE),
        ("privacy_policy", const.SYS_CONFIG_PRIVACY_POLICY),
        ("legal_notice", const.SYS_CONFIG_LEGAL_NOTICE),
    ],
)
def test_legals(endpoint, config_item, client):
    """Test the endpoints related to displaying legal notices."""
    endpoint = url_for(f"main.{endpoint}")

    response = client.get(endpoint)
    check_view_response(response, status_code=302)

    set_sys_config(config_item, "Test")

    response = client.get(endpoint)
    check_view_response(response)


def test_oauth2_server_authorize_error(monkeypatch, client, user_session):
    """Test an invalid auth request to the "main.oauth2_server_authorize" endpoint."""
    monkeypatch.setitem(os.environ, "AUTHLIB_INSECURE_TRANSPORT", "true")

    with user_session():
        response = client.get(url_for("main.oauth2_server_authorize"))

        check_view_response(response, status_code=400)
        assert "invalid authorization request" in response.get_data(as_text=True)


@pytest.mark.parametrize("include_prefix", [True, False])
@pytest.mark.parametrize("token_type", ["access_token", "refresh_token"])
@pytest.mark.parametrize("token_type_hint", ["access_token", "refresh_token", None])
def test_oauth2_server_revoke(
    monkeypatch,
    include_prefix,
    token_type,
    token_type_hint,
    client,
    new_oauth2_server_client,
    new_oauth2_server_token,
):
    """Test the "main.oauth2_server_revoke" endpoint."""
    monkeypatch.setitem(os.environ, "AUTHLIB_INSECURE_TRANSPORT", "true")

    client_secret = OAuth2ServerClient.new_client_secret()
    oauth2_server_client = new_oauth2_server_client(client_secret=client_secret)

    access_token = OAuth2ServerToken.new_access_token(include_prefix=include_prefix)
    refresh_token = OAuth2ServerToken.new_refresh_token()
    new_oauth2_server_token(
        client=oauth2_server_client,
        access_token=access_token,
        refresh_token=refresh_token,
    )

    data = {
        "client_id": oauth2_server_client.client_id,
        "client_secret": client_secret,
    }

    if token_type_hint is not None:
        data["token_type_hint"] = token_type_hint

    if token_type == "access_token":
        data["token"] = access_token
    else:
        data["token"] = refresh_token

    response = client.post(url_for("main.oauth2_server_revoke"), data=data)

    # The response should always use status code 200 according to the standard.
    check_api_response(response)

    # Only if the token type hint is given but does not match the actual token type the
    # revocation should fail.
    if token_type_hint is not None and token_type_hint != token_type:
        assert OAuth2ServerToken.query.one()
    else:
        assert not OAuth2ServerToken.query.all()


def _check_token_data(token_data, scope):
    expires_in = current_app.config["OAUTH2_TOKEN_EXPIRES_IN"][
        const.OAUTH_GRANT_AUTH_CODE
    ]

    assert "access_token" in token_data
    assert "refresh_token" in token_data
    assert "expires_in" in token_data
    assert token_data["expires_in"] == expires_in
    assert "token_type" in token_data
    assert token_data["token_type"] == const.OAUTH_TOKEN_TYPE
    assert "scope" in token_data
    assert token_data["scope"] == scope


def test_oauth2_server_flow(
    monkeypatch, client, new_oauth2_server_client, user_session
):
    """Test the complete OAuth2 flow with all currently registered grants."""
    monkeypatch.setitem(os.environ, "AUTHLIB_INSECURE_TRANSPORT", "true")

    redirect_uri = "https://foo.bar/callback"
    scope = "record.read"
    client_secret = OAuth2ServerClient.new_client_secret()
    state = random_alnum()

    # Used for PKCE when using the authorization code flow.
    code_verifier = random_alnum(num_chars=50)
    code_challenge = create_s256_code_challenge(code_verifier)
    code_challenge_method = "S256"

    oauth2_server_client = new_oauth2_server_client(
        redirect_uris=[redirect_uri], scope=scope, client_secret=client_secret
    )

    authorize_endpoint = url_for(
        "main.oauth2_server_authorize",
        response_type=const.OAUTH_RESPONSE_TYPE,
        client_id=oauth2_server_client.client_id,
        redirect_uri=redirect_uri,
        state=state,
        code_challenge=code_challenge,
        code_challenge_method=code_challenge_method,
    )

    with user_session():
        # Initiate the OAuth2 flow by asking the user for consent.
        response = client.get(authorize_endpoint)

        check_view_response(response)
        assert (
            "An application is requesting access to your account"
            in response.get_data(as_text=True)
        )

        # If the user does not grant consent, they should be redirected back with a
        # corresponding error.
        response = client.post(authorize_endpoint, data={"submit_deny": True})
        check_view_response(response, status_code=302)

        result = urlparse(response.location)
        qparams = parse_qs(result.query)

        assert "error" in qparams
        assert qparams["error"][0] == "access_denied"
        assert not OAuth2ServerAuthCode.query.all()

        # If the user does grant consent, they should be redirected back with an
        # authorization code and the state that was sent originally.
        response = client.post(authorize_endpoint, data={"submit_auth": True})
        check_view_response(response, status_code=302)

        result = urlparse(response.location)
        qparams = parse_qs(result.query)

        assert "code" in qparams
        assert "state" in qparams
        assert qparams["state"][0] == state

        code = qparams["code"][0]
        oauth2_server_code = OAuth2ServerAuthCode.query.one()

        assert oauth2_server_code.code == code
        assert oauth2_server_code.scope == oauth2_server_client.scope

    # Now we should be able to exchange the authorization code for a token, which should
    # also remove the authorization code again.
    response = client.post(
        url_for("main.oauth2_server_token"),
        data={
            "grant_type": const.OAUTH_GRANT_AUTH_CODE,
            "code": code,
            "client_id": oauth2_server_client.client_id,
            "client_secret": client_secret,
            "redirect_uri": redirect_uri,
            "code_verifier": code_verifier,
        },
    )
    check_api_response(response)

    token_data = response.get_json()
    _check_token_data(token_data, scope)

    assert not OAuth2ServerAuthCode.query.all()

    oauth2_server_token = OAuth2ServerToken.query.one()

    assert oauth2_server_token.scope == oauth2_server_client.scope

    # Now we can also request a new token with the refresh token. This should also
    # remove the previous token.
    refresh_token = token_data["refresh_token"]

    response = client.post(
        url_for("main.oauth2_server_token"),
        data={
            "grant_type": const.OAUTH_GRANT_REFRESH_TOKEN,
            "refresh_token": refresh_token,
            "client_id": oauth2_server_client.client_id,
            "client_secret": client_secret,
        },
    )
    check_api_response(response)

    token_data = response.get_json()
    _check_token_data(token_data, scope)

    new_oauth2_server_token = OAuth2ServerToken.query.one()

    assert new_oauth2_server_token != oauth2_server_token
    assert new_oauth2_server_token.scope == oauth2_server_client.scope

    # Now we change the scope of the client and try requesting another token again using
    # the new refresh token, which should fail and lead to the token being removed.
    refresh_token = token_data["refresh_token"]
    oauth2_server_client.update_client_metadata(scope=None)

    response = client.post(
        url_for("main.oauth2_server_token"),
        data={
            "grant_type": const.OAUTH_GRANT_REFRESH_TOKEN,
            "refresh_token": refresh_token,
            "client_id": oauth2_server_client.client_id,
            "client_secret": client_secret,
        },
    )

    check_api_response(response, status_code=400)

    error_data = response.get_json()

    assert error_data["error"] == "invalid_grant"
    assert not OAuth2ServerToken.query.all()
