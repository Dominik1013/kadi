# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from io import BytesIO

import pytest

import kadi.lib.constants as const
from kadi.lib.exceptions import KadiFilesizeExceededError
from kadi.lib.security import hash_value
from kadi.lib.storage.local import LocalStorage


def test_local_storage(tmp_path, request_context):
    """Test if the local storage works correctly."""

    # Try creating a storage with an invalid root directory.
    with pytest.raises(ValueError):
        LocalStorage("test")

    storage = LocalStorage(tmp_path)

    # Try using the storage with invalid identifiers.
    with pytest.raises(ValueError):
        storage.exists(os.path.join("te", "st"))

    with pytest.raises(ValueError):
        storage.exists("test")

    identifier = "abcdefg"
    test_data = 10 * b"x"

    # Try saving too much data.
    with pytest.raises(KadiFilesizeExceededError):
        storage.save(identifier, BytesIO(test_data), max_size=9)

    # Try saving a valid amount of data.
    checksum = storage.save(identifier, BytesIO(test_data), max_size=10)

    assert os.path.isfile(os.path.join(storage.root_directory, "ab", "cd", "ef", "g"))
    assert checksum == hash_value(test_data, alg="md5")

    # Check if the saved file exists according to the storage.
    assert storage.exists(identifier)

    # Try retrieving the size of the file.
    assert storage.get_size(identifier) == len(test_data)

    # Try determining the MIME type of the file.
    assert storage.get_mimetype(identifier) == const.MIMETYPE_TEXT

    # Try opening and reading from the saved file.
    with storage.open(identifier) as f:
        assert f.read() == test_data

    identifier2 = "gfedcba"

    # Try moving the file to a different location.
    storage.move(identifier, identifier2)

    assert storage.exists(identifier2)
    assert not storage.exists(identifier)
    assert os.listdir(storage.root_directory) == ["gf"]

    identifier3 = f"{identifier2}2"

    # Try merging two files into one.
    storage.save(identifier3, BytesIO(test_data))
    storage.merge(identifier, [identifier2, identifier3])

    assert storage.exists(identifier)

    with storage.open(identifier) as f:
        assert f.read() == 2 * test_data

    # Try deleting the two merged files.
    storage.delete(identifier2)
    storage.delete(identifier3)

    assert not storage.exists(identifier2)
    assert not storage.exists(identifier3)
    assert os.listdir(storage.root_directory) == ["ab"]

    # Try downloading the remaining file.
    filename = "test"
    response = storage.download(
        identifier, filename=filename, mimetype=const.MIMETYPE_TEXT
    )

    assert response.status_code == 200
    assert response.content_type == f"{const.MIMETYPE_TEXT}; charset=utf-8"
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"
    assert response.response.file.read() == 2 * test_data

    response.close()
