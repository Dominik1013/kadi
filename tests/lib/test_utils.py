# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.utils import SimpleReprMixin
from kadi.lib.utils import StringEnum
from kadi.lib.utils import as_list
from kadi.lib.utils import find_dict_in_list
from kadi.lib.utils import flatten_list
from kadi.lib.utils import get_class_by_name
from kadi.lib.utils import is_http_url
from kadi.lib.utils import is_iterable
from kadi.lib.utils import is_quoted
from kadi.lib.utils import named_tuple
from kadi.lib.utils import rgetattr
from kadi.modules.records.models import Record


def test_simple_repr_mixin():
    """Test if the "SimpleReprMixin" works correctly."""

    class _Test(SimpleReprMixin):
        # pylint: disable=missing-class-docstring
        class Meta:
            representation = ["a", "b"]

        a = 1
        b = 2

    assert repr(_Test()) == "_Test(a=1, b=2)"


def test_string_enum():
    """Test if the "StringEnum" works correctly."""

    class _Test(StringEnum):
        pass

    assert _Test.__values__ == []

    class _Test(StringEnum):
        __values__ = ["a", "b"]

    assert _Test.__values__ == ["a", "b"]
    assert _Test.A == "a"
    assert _Test.B == "b"


def test_named_tuple():
    """Test if the "named_tuple" shortcut works correctly."""
    test_tuple = named_tuple("Test", a=1, b=2)

    assert test_tuple.a == 1
    assert test_tuple.b == 2


def test_rgetattr():
    """Test if recursively getting an attribute works correctly."""
    test_obj = named_tuple("Test", a=named_tuple("Test2", b="test"))

    assert rgetattr(test_obj, "a.b") == "test"
    assert rgetattr(test_obj, "a.b.c", "default") == "default"


@pytest.mark.parametrize(
    "name,cls",
    [("kadi.modules.records.models.Record", Record), ("kadi.invalid.name", None)],
)
def test_get_class_by_name(name, cls):
    """Test if getting a class by its name works correctly."""
    assert get_class_by_name(name) == cls


@pytest.mark.parametrize(
    "value,include_string,result",
    [
        (1, False, False),
        ("test", False, False),
        ("test", True, True),
        ([], False, True),
        ({}, False, True),
    ],
)
def test_is_iterable(value, include_string, result):
    """Test if iterables are detected correctly."""
    assert is_iterable(value, include_string=include_string) is result


@pytest.mark.parametrize(
    "value,result",
    [
        ("", False),
        ("a", False),
        ('"a', False),
        ('a"', False),
        ('""', True),
        ('"a"', True),
    ],
)
def test_is_quoted(value, result):
    """Test if quoted strings are detected correctly."""
    assert is_quoted(value) is result


@pytest.mark.parametrize(
    "value,result",
    [
        ("", False),
        ("test", False),
        ("http://", False),
        ("http://test", True),
        ("https://test", True),
        ("https://test.test/test?test=test#test", True),
    ],
)
def test_is_http_url(value, result):
    """Test if HTTP URLs are detected correctly."""
    assert is_http_url(value) is result


def test_find_dict_in_list():
    """Test if dictionaries in lists are found correctly."""
    dict_list = [{"a": 1}, {"b": None}]

    assert find_dict_in_list(dict_list, "a", 1) == {"a": 1}
    assert find_dict_in_list(dict_list, "b", None) == {"b": None}
    assert find_dict_in_list(dict_list, "c", None) is None


@pytest.mark.parametrize(
    "value,result",
    [
        ([], []),
        ([1, 2, 3], [1, 2, 3]),
        ([1, 2, [3]], [1, 2, 3]),
        ([[1, 2], [3]], [1, 2, 3]),
    ],
)
def test_flatten_list(value, result):
    """Test if lists are flattened correctly."""
    assert flatten_list(value) == result


@pytest.mark.parametrize(
    "value,result",
    [("foo", ["foo"]), (["foo"], ["foo"]), ([], []), (None, None)],
)
def test_as_list(value, result):
    """Test if values are wrapped inside lists correctly."""
    assert as_list(value) == result
