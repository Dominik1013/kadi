# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# The following code is a slightly modified version of httpdomain's autoflask directive
# and corresponding utility functions, which are licensed under the two-clause BSD
# license and available at:
#
# https://github.com/sphinx-contrib/httpdomain/tree/1.8.1/sphinxcontrib/autohttp
#
# Copyright (c) 2010 by the contributors (see AUTHORS file).
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import itertools
import re

from docutils.parsers.rst import directives
from marshmallow import fields
from sphinx.util.docstrings import prepare_docstring as _prepare_docstring
from sphinxcontrib.autohttp.common import import_object
from sphinxcontrib.autohttp.flask import AutoflaskDirective
from sphinxcontrib.autohttp.flask_base import AutoflaskBase as _AutoflaskBase
from sphinxcontrib.autohttp.flask_base import get_routes
from werkzeug.http import HTTP_STATUS_CODES

import kadi.lib.constants as const
from kadi.lib.schemas import CustomString

import json


SCHEMA_TYPE_MAPPING = {
    CustomString: "String",
    fields.String: "String",
    fields.Integer: "Integer",
    fields.Float: "Float",
    fields.Boolean: "Boolean",
    fields.DateTime: "Datetime",
}

SCHEMA_TYPE_FALLBACK = "Object"


def _reqschema(fields, indent=4):
    for name, meta in fields.items():
        line = f"{indent * ' '}* **{name}**"

        if meta.get("required", False):
            line += " (*Required*)"

        if meta.get("many", False):
            line += f" - Array [{meta['type']}]"
        else:
            line += f" - {meta['type']}"

        yield line

        if "nested" in meta:
            yield ""
            yield from _reqschema(meta["nested"], indent + 2)
            yield ""


def _get_field_attr(field, attr, default=None):
    # Try to retrieve the attribute from the custom field metadata first.
    if attr in field.metadata:
        return field.metadata[attr]

    if hasattr(field, attr):
        return getattr(field, attr)

    return default


def _get_reqschema_fields(schema, is_partial=False):
    fields_meta = {}

    for name, field in schema.fields.items():
        if field.dump_only:
            continue

        field_meta = {
            "required": _get_field_attr(field, "required", False),
            "many": _get_field_attr(field, "many", False),
            "type": _get_field_attr(
                field,
                "type",
                SCHEMA_TYPE_MAPPING.get(field.__class__, SCHEMA_TYPE_FALLBACK),
            ),
        }

        is_partial = is_partial or (
            schema.partial is True
            or (isinstance(schema.partial, tuple) and name in schema.partial)
        )

        if is_partial:
            field_meta["required"] = False

        if isinstance(field, fields.Pluck):
            field_meta["type"] = SCHEMA_TYPE_MAPPING.get(
                field.schema.fields[field.field_name].__class__,
                SCHEMA_TYPE_FALLBACK,
            )
        elif isinstance(field, fields.Nested):
            # Pass along whether the schema is loaded partially, as we can't retrieve
            # this info from the nested schema directly.
            field_meta["nested"] = _get_reqschema_fields(
                field.schema, is_partial=is_partial
            )

        fields_meta[name] = field_meta

    sorted_fields = sorted(fields_meta.items(), key=lambda field: field[0])
    sorted_fields = sorted(
        sorted_fields, key=lambda field: field[1]["required"], reverse=True
    )

    return dict(sorted_fields)


def _reqheaders(headers):
    for name, meta in headers.items():
        line = f"    * **{name}**"

        if meta.get("required", False):
            line += " (*Required*)"

        if "description" in meta:
            line += f" - {meta['description']}"

        yield line


def _replace_api_version(value, version=None):
    # Ensure that references to other endpoints point to the correct API version.
    # if version:
    #     ref_prefix = "</api"
    #     return value.replace(ref_prefix, f"{ref_prefix}/{version}")

    return value


def http_directive(method, paths, docstring, apidoc_meta, version):
    # Method and endpoint.
    method = method.lower().strip()
    paths = [paths] if isinstance(paths, str) else paths

    for path in paths:
        yield f".. http:{method}:: {path}"

    # Docstring.
    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    yield ""
    for line in docstring:
        yield f"  {line}"

    # Required scopes.
    if const.APIDOC_SCOPES_KEY in apidoc_meta:
        scopes = apidoc_meta[const.APIDOC_SCOPES_KEY]["scopes"]
        operator = apidoc_meta[const.APIDOC_SCOPES_KEY]["operator"]

        required_scopes = f"`` *{operator}* ``".join(scopes)

        yield ""
        yield f"  **Required scopes:** ``{required_scopes}``"

    # Query parameters.
    if (
        const.APIDOC_QPARAMS_KEY in apidoc_meta
        or const.APIDOC_PAGINATION_KEY in apidoc_meta
    ):
        yield ""
        yield "  **Query parameters**"
        yield ""

        # Pagination.
        pagination_meta = apidoc_meta.get(const.APIDOC_PAGINATION_KEY)

        if pagination_meta:
            page = "    * **page** - The current result page"

            if pagination_meta["page_max"]:
                page += f", limited to a maximum of ``{pagination_meta['page_max']}``."
            else:
                page += "."

            page += " (*Default:* ``1``)"
            yield page

            per_page = "    * **per_page** - Number of results per page"

            if pagination_meta["per_page_max"]:
                per_page += (
                    f", limited to a maximum of ``{pagination_meta['per_page_max']}``."
                )
            else:
                page += "."

            per_page += " (*Default:* ``10``)"
            yield per_page

        # Others.
        for name, qparam_meta in apidoc_meta.get(const.APIDOC_QPARAMS_KEY, {}).items():
            qparam = f"    * **{name}** - {qparam_meta['description']}"

            if qparam_meta["multiple"]:
                qparam += " (*Can be specified more than once.*)"
            else:
                default = qparam_meta["default"]

                # Ignore None values and empty strings.
                if default is not None and default != "":
                    qparam += f" (*Default:* ``{default}``)"

            yield qparam

    # JSON request body via a marshmallow schema.
    if const.APIDOC_REQ_SCHEMA_KEY in apidoc_meta:
        reqschema_meta = apidoc_meta[const.APIDOC_REQ_SCHEMA_KEY]

        yield ""
        yield "  **Request JSON object**"
        yield ""

        if reqschema_meta["description"]:
            yield f"    {reqschema_meta['description']}"
            yield ""

        fields = _get_reqschema_fields(reqschema_meta["schema"])
        yield from _reqschema(fields)

    # Custom request headers.
    if const.APIDOC_REQ_HEADERS_KEY in apidoc_meta:
        reqheaders_meta = apidoc_meta[const.APIDOC_REQ_HEADERS_KEY]

        sorted_headers = sorted(reqheaders_meta.items(), key=lambda field: field[0])
        sorted_headers = sorted(
            sorted_headers,
            key=lambda field: field[1].get("required", False),
            reverse=True,
        )

        yield ""
        yield "  **Custom request headers**"
        yield ""
        yield from _reqheaders(dict(sorted_headers))

    # Status codes.
    if const.APIDOC_STATUS_CODES_KEY in apidoc_meta:
        yield ""
        yield "  **Status codes**"
        yield ""

        for status, description in apidoc_meta[const.APIDOC_STATUS_CODES_KEY].items():
            description = _replace_api_version(description, version)
            yield (
                f"    * **{status}** (*{HTTP_STATUS_CODES.get(status, 'Unknown')}*)"
                f" - {description}"
            )

    yield ""


def quickref_directive(method, path, docstring):
    method = method.lower().strip()

    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    if len(docstring) > 0:
        description = docstring[0]
    else:
        description = ""

    ref = path.replace("<", "(").replace(">", ")").replace("/", "-").replace(":", "-")

    yield f"    * - `{method.upper()} {path} <#{method.lower()}-{ref}>`_"
    yield f"      - {description}"


def prepare_docstring(docstring, version=None):
    docstring = _replace_api_version(docstring, version=version)
    return _prepare_docstring(docstring)


class AutoflaskBase(_AutoflaskBase):
    @property
    def version(self):
        return self.options.get("version")

    @property
    def tags(self):
        tags = self.options.get("tags")

        if not tags:
            return None

        return re.split(r"\s*,\s*", tags)

    @property
    def methods(self):
        methods = self.options.get("methods")

        if not methods:
            return None

        return [m.lower() for m in re.split(r"\s*,\s*", methods)]

    def get_routes_iter(self, app):
        routes = self.inspect_routes(app)

        if "view" in self.groupby:
            routes = self.groupby_view(routes)

        return routes
    
    def make_rst(self, qref=False):
        # Lade die OpenAPI-Spezifikationsdatei
        with open(self.arguments[0]) as f:
            openapi_spec = json.load(f)
        
        selected_methods = self.methods
        
        filtered_spec = self.filter_openapi_spec_by_methods(openapi_spec, selected_methods)
         
        # Optionale Schnellübersicht (autoquickref)
        autoquickref = self.options.get("autoquickref", False) is None                
        if autoquickref:
            yield ""
            yield ".. list-table::"
            yield "    :class: narrow-table"
            yield ""

            # Generiere die Schnellübersicht der Endpunkte und Methoden
            for path, path_item in filtered_spec.get("paths", {}).items():
                for method, operation in path_item.items():
                    docstring = operation.get("description", "")
                    prepared_docstring = prepare_docstring(docstring, filtered_spec["info"].get("version"))
                    yield from quickref_directive(method, path, prepared_docstring)
            yield ""

        # Detailübersicht für jeden Endpunkt und jede Methode
        for path, path_item in filtered_spec.get("paths", {}).items():
            for method, operation in path_item.items():
                # Wähle die richtige Direktive basierend auf der HTTP-Methode
                http_directive_type = f"http:{method.lower()}"

                # Ausgabe der Direktive und des Endpunkts
                yield f".. {http_directive_type}:: {path}"
                yield ""

                
                
                # Optional: Kurze Beschreibung des Endpunkts
                summary = operation.get("description")
                if summary:
                    yield f"    {summary}"
                    yield ""  # Leerzeile nach Beschreibung für Sphinx

                scope = operation.get("scope", {})
                if scope:
                    scopes = scope.get("scopes", [])
                    yield "    **Required scopes:**"
                    yield ""
                    for s in scopes:
                        yield f"        ``{s}``"
                        yield ""
                        
                # Parameter aus der OpenAPI-Spezifikation dokumentieren
                parameters = operation.get("parameters", [])
                if parameters:
                    yield "    **Query parameters:**"
                    yield ""
                    for param in parameters:
                        param_name = param.get("name", "Unbenannt")
                        param_desc = param.get("description", "Keine Beschreibung")
                        yield f"        * **{param_name}**: {param_desc}"
                    yield ""
                  

                # Request-Body aus der OpenAPI-Spezifikation dokumentieren
                request_body = operation.get("requestBody")
                if request_body:
                    yield "    **Request JSON object:**"
                    yield ""
                    content = request_body.get("content", {}).get("application/json", {})
                    schema = content.get("schema", {})
                    body_description = schema.get("description")
                    if body_description:
                        yield f"    {body_description}"
                        yield ""  # Leerzeile nach Request-Body-Beschreibung
                    yield ""

                    # Ausgabe der Eigenschaften (properties) des JSON-Objekts
                    properties = schema.get("properties", {})
                    required_fields = schema.get("required", [])
                    for prop, details in properties.items():
                        prop_type = details.get("type", "unbekannter Typ")
                        is_required = " (required)" if prop in required_fields else ""
                        yield f"        * **{prop}**{is_required} - {prop_type}"
                        yield ""  # Leerzeile nach Eigenschaften
                    yield ""

                # Responses aus der OpenAPI-Spezifikation dokumentieren
                responses = operation.get("responses", {})
                if responses:
                    yield "    **Status codes:**"
                    yield ""
                    for status_code, response in responses.items():
                        response_desc = response.get("description", "Keine Beschreibung")
                        yield f"        * **{status_code}** {response_desc}"
                        content = response.get("content", {}).get("application/json", {})
                        schema = content.get("schema", {})
                        if schema:
                            yield f"    **Response JSON object for status {status_code}:**"
                            yield ""
                            properties = schema.get("properties", {})
                            required_fields = schema.get("required", [])
                            for prop, details in properties.items():
                                prop_type = details.get("type", "unbekannter Typ")
                                is_required = " (required)" if prop in required_fields else ""
                                yield f"        * **{prop}**{is_required} - {prop_type}"
                                yield ""  # Leerzeile nach Response-Eigenschaften
                            yield ""

                yield ""  # Leerzeile zur Trennung der Methoden für bessere Lesbarkeit



        # app = import_object(self.arguments[0])
        # print(self.arguments)
        
        # autoquickref = self.options.get("autoquickref", False) is None

        # if autoquickref:
        #     yield ""
        #     yield ".. list-table::"
        #     yield "    :class: narrow-table"
        #     yield ""

        #     routes_iter = self.get_routes_iter(app)

        #     for method, paths, view_func, view_doc in routes_iter:
        #         docstring = prepare_docstring(view_doc, self.version)

        #         for path in paths:
        #             yield from quickref_directive(method, path, docstring)

        # routes_iter = self.get_routes_iter(app)

        # for method, paths, view_func, view_doc in routes_iter:
        #     docstring = prepare_docstring(view_doc, self.version)
        #     apidoc_meta = getattr(view_func, const.APIDOC_META_ATTR, {})

        #     yield from http_directive(
        #         method, paths, docstring, apidoc_meta, self.version
        #     )
    import re

    import re

    def filter_openapi_spec_by_methods(self, openapi_spec, selected_methods):
        filtered_paths = {}

        for path, path_item in openapi_spec.get("paths", {}).items():
            # Zuerst wird nach den ausgewählten Methoden gefiltert
            filtered_methods = {
                method: details
                for method, details in path_item.items()
                if method.lower() in selected_methods
            }

            if filtered_methods:
                # Wenn eine Version angegeben ist, wird auf die Version der operationId geachtet
                if self.version:
                    filtered_methods = {
                        method: details
                        for method, details in filtered_methods.items()
                        if 'operationId' in details and re.search(f"_{self.version}$", details['operationId'])
                    }

                # Wenn keine Version angegeben ist, wird die Filterung für versionierte Endpunkte vorgenommen
                elif any('operationId' in details and re.search("_v[0-9]+$", details['operationId']) for details in filtered_methods.values()):
                    filtered_methods = {
                        method: details
                        for method, details in filtered_methods.items()
                        if 'operationId' in details and not re.search("_v[0-9]+$", details['operationId'])
                    }

                if filtered_methods:
                    filtered_paths[path] = filtered_methods

        return {
            "openapi": openapi_spec.get("openapi"),
            "info": openapi_spec.get("info"),
            "paths": filtered_paths,
        }

        
    def inspect_routes(self, app):
        order = self.order or "path"

        if self.endpoints:
            routes = itertools.chain(
                *(get_routes(app, endpoint, order) for endpoint in self.endpoints)
            )
        else:
            routes = get_routes(app, order=order)

        for method, paths, endpoint in routes:
            try:
                blueprint, _, _ = endpoint.rpartition(".")

                if (
                    self.blueprints and blueprint not in self.blueprints
                ) or blueprint in self.undoc_blueprints:
                    continue

            except ValueError:
                pass

            if endpoint == "static" or endpoint in self.undoc_endpoints:
                continue

            view = app.view_functions[endpoint]

            if (self.modules and view.__module__ not in self.modules) or (
                self.undoc_modules and view.__module__ in self.modules
            ):
                continue

            view_class = getattr(view, "view_class", None)

            if view_class is None:
                view_func = view
            else:
                view_func = getattr(view_class, method.lower(), None)

            # Filter out internal and experimental endpoints.
            apidoc_meta = getattr(view_func, const.APIDOC_META_ATTR, {})

            if apidoc_meta.get(const.APIDOC_EXPERIMENTAL_KEY, False) or apidoc_meta.get(
                const.APIDOC_INTERNAL_KEY, False
            ):
                continue

            # Filter out endpoints not being in the specified packages.
            skip_endpoint = True

            if self.packages:
                for package in self.packages:
                    if view.__module__.startswith(package):
                        skip_endpoint = False
                        break

                if skip_endpoint:
                    continue

            # Filter out endpoints not having the correct HTTP methods.
            if self.methods and method.lower() not in self.methods:
                continue

            # Filter out endpoints not belonging to the given API version.
            if self.version:
                if not re.search(f"^.*_{self.version}$", endpoint):
                    continue
            # Otherwise, skip all versioned endpoints.
            else:
                if re.search("^.*_v[0-9]+$", endpoint):
                    continue

            view_doc = view.__doc__ or ""

            if view_func and view_func.__doc__:
                view_doc = view_func.__doc__

            if not view_doc and "include-empty-docstring" not in self.options:
                continue

            yield (method, paths, view_func, view_doc)


class Autoflask(AutoflaskBase, AutoflaskDirective):
    """Modified ``autoflask`` directive.

    Has some modified output, default values and filters and additionally provides the
    following options:

    * ``:version:`` Specify a single API version that should be documented (based on the
      associated endpoints). If the option is not provided, all unversioned endpoints
      (which automatically point to the latest version) will be taken instead.
    * ``:packages:`` Limits the documented endpoints to those in the specified packages
      and its subpackages. Multiple packages can be given separated by commas.
    * ``:methods:`` Limits the documented endpoints by specified HTTP methods. Multiple
      methods can be given separated by commas.

    **Example:**

    .. code-block:: rst

        .. autoflask:: kadi.wsgi:app
            :version: v1
            :packages: kadi.modules.records.api
            :methods: get, post
    """


Autoflask.option_spec["version"] = directives.unchanged
Autoflask.option_spec["tags"] = directives.unchanged
Autoflask.option_spec["methods"] = directives.unchanged
Autoflask.option_spec["file_path"] = directives.unchanged


def setup(app):
    app.add_directive("autoflask", Autoflask)
