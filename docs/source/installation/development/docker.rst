.. _installation-development-docker:

Using Docker containers
=======================

The :ref:`installation instructions <installation-development-installation>` assume that
all services |kadi| depends on will be installed directly. When planning to run some
services as Docker containers instead during development, the `Docker Engine
<https://docs.docker.com/engine>`__ needs to be installed first. This can be achieved by
running the following commands:

.. code-block:: bash

    sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release
    sudo install -m 0755 -d /etc/apt/keyrings

    # When using Debian
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    # When using Ubuntu
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

Running the services
--------------------

Once Docker is installed, the containers of the corresponding services can be created
and started as explained in following sections. For more information, such as stopping,
restarting or retrieving the logs of the containers, please refer to the official
documentation of `Docker <https://docs.docker.com/engine>`__.

PostgreSQL
~~~~~~~~~~

PostgreSQL can be run via the following command, which will create and start a container
called ``postgres``, expose port 5432 of the container and persist all data in a named
volume also called ``postgres``:

.. code-block:: bash

    sudo docker run --name postgres -d -p 5432:5432 -v postgres:/var/lib/postgresql/data -e POSTGRES_DB=kadi -e POSTGRES_USER=kadi -e POSTGRES_PASSWORD=kadi postgres:16

The environment variables specified via this command correspond to the database settings
that are also used in the :ref:`installation instructions
<installation-development-installation>` and can be adjusted as necessary.

Redis
~~~~~

Redis can be run via the following command, which will create and start a container
called ``redis``, expose port 6379 of the container and persist all data in a named
volume also called ``redis``:

.. code-block:: bash

    sudo docker run --name redis -d -p 6379:6379 -v redis:/data redis:7

Elasticsearch
~~~~~~~~~~~~~

Elasticsearch can be run via the following command, which will create and start a
container called ``elasticsearch``, expose port 9200 of the container and persist all
data in a named volume also called ``elasticsearch``:

.. code-block:: bash

    sudo docker run --name elasticsearch -d -p 9200:9200 -v elasticsearch:/usr/share/elasticsearch/data -e discovery.type=single-node -e xpack.security.enabled=false -e xpack.security.transport.ssl.enabled=false -e xpack.security.http.ssl.enabled=false -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" elasticsearch:8.13.4
