.. _installation-development-wsl:

Developing under WSL
====================

On Windows, it is possible to develop |kadi| using the `Windows Subsystem for Linux
<https://learn.microsoft.com/en-us/windows/wsl>`__ (WSL), which allows one to run a
Linux environment without the need for a separate virtual machine or dual booting. This
chapter focuses on some WSL-specific differences to the regular :ref:`development
installation <installation-development>` instructions.

.. note::
    Some of the instructions explained in this chapter may require the use of WSL 2
    and/or Windows 11.

systemd
-------

As the installation instructions make use of systemd for managing background services,
it is recommended to setup systemd in WSL as well, if not already the case. Doing so
should only require adding the following lines to the file ``/etc/wsl.conf`` (which
might have to be created first) within the WSL and restarting it afterwards:

.. code-block:: ini

    [boot]
    systemd=true

Development environments
------------------------

When choosing a development environment to use in combination with WSL, the easiest
option is Visual Studio Code (VS Code) in combination with its WSL extension, as
explained in the official `documentation
<https://code.visualstudio.com/docs/remote/wsl>`__. This allows one to keep working
under Windows, while the code itself and all additional tools stay and/or run within the
WSL.

.. tip::
    Using other development environments or text editors in this way is also possible,
    but will usually require setting up an X server on the Windows side and configuring
    it to be used by the WSL or similar setups.
