.. _installation-configuration:

Configuration
=============

This section explains all configuration items that |kadi| supports. The configuration is
specified via a corresponding configuration file. The location of this file (e.g.
usually ``/opt/kadi/config/kadi.py`` for productive installations) and which of the
configuration items may need to be changed in order to make |kadi| work properly depends
on the installation method. It is therefore recommended to have a working (or work in
progress) |kadi| :ref:`installation <installation>` before reading through this section.

Note that the configuration file of |kadi| is a normal Python file. This means that it
always has to be syntactically correct. A basic example of a configuration file changing
the values of two configuration items (as further explained in the configuration
:ref:`reference <installation-configuration-reference>`) may look like the following:

.. code-block:: python3

    # Configure authentication providers to log in to Kadi4Mat.
    AUTH_PROVIDERS = [
        {
            "type": "local",
            "allow_registration": True,
        },
    ]

    # Configure the name of the Kadi4Mat server.
    SERVER_NAME = "kadi4mat.example.edu"

    # Other configuration values...

.. note::
    Whenever changing the configuration file in production environments once |kadi| is
    installed successfully, it may be necessary to restart the application and/or
    dependent services for any changes to take effect, similar to performing an
    :ref:`update <installation-production-updating>`.

.. _installation-configuration-sysadmins:

Sysadmins
---------

Some configurations can also be adjusted via a graphical sysadmin interface, which also
does not require a restart of the application. This interface can be found below the
*Settings* menu item in the dropdown menu on the far right of the navigation bar in
|kadi|, once a user is set as a sysadmin. To initially set a user as sysadmin, the Kadi
CLI can be used after installing and configuring |kadi| successfully:

.. code-block:: bash

    sudo su - kadi                # Switch to the kadi user (in production environments)
    kadi users sysadmin <user_id> # Set the user given by its ID via <user_id> as sysadmin

If not already known, the (persistent) ID of a user can be retrieved via the web
interface on the overview page of the corresponding user.

.. note::
    Configuration values set via this interface take precedence over any values
    specified via a configuration file.

.. warning::
    Sysadmins should be limited to users that are trustworthy (e.g. users that already
    have server access anyways), as sysadmins can change |kadi|'s configuration and
    manage users and their resources.

.. _installation-configuration-reference:

Reference
---------

The following sections contain the configuration reference related to
:ref:`authentication <installation-configuration-auth>`, :ref:`storage
<installation-configuration-storage>` and other :ref:`settings
<installation-configuration-settings>`. Note that all listed default values correspond
to the ones used for production environments.

.. toctree::
    :maxdepth: 1

    auth
    storage
    settings
