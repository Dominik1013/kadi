.. _httpapi-endpoints:

Endpoints
---------

The following sections document the actual endpoints that the HTTP API of |kadi|
provides, grouped by API version. The documentation of each endpoint includes required
access token scopes, query parameters and/or request data as well as status codes
specific to the endpoint. If not otherwise noted, the common request/response formats
and status codes, as explained in the :ref:`introduction <httpapi-intro>` section, apply
for each of the documented endpoints.

.. toctree::
    :maxdepth: 2

    v1



