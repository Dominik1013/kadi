Useful tools
============

.. _development-tools-kadi-cli:

Kadi CLI
~~~~~~~~

The Kadi command line interface (CLI) contains various useful tools and utilities as
part of mulitple subcommands and is available automatically after installing the
package. An overview over all commands can be obtained by simply running:

.. code-block:: bash

    kadi

The Kadi CLI ensures that each subcommand runs inside the context of the application,
which is why it always needs access to the correct |kadi| environment and, if
applicable, configuration file. Please see the |kadi| :ref:`configuration section
<installation-development-installation-configuration-kadi4mat>` in the development
installation instructions for a reminder. For this reason, some subcommands are simply
wrappers over existing ones provided by other libraries, making their use in certain
scenarios or environments easier. See also :ref:`cli <development-overview-cli>`.

.. _development-tools-virtualenvwrapper:

virtualenvwrapper
~~~~~~~~~~~~~~~~~

`virtualenvwrapper <https://virtualenvwrapper.readthedocs.io>`__ is an extension to the
Virtualenv tool and can be used to manage and switch between multiple virtual
environments more easily. The tool can be installed globally via pip while not having
any virtual environment currently active:

.. code-block:: bash

    pip3 install virtualenvwrapper

Afterwards, some environment variables have to be set. Generally, a suitable place for
them is the ``~/.bashrc`` file. An example could look like the following:

.. code-block:: bash

    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export WORKON_HOME=${HOME}/.venvs
    source ${HOME}/.local/bin/virtualenvwrapper.sh

Please refer to the official documentation about their meaning as well as other possible
variables that can be used, as their values differ by system and personal preference.

EditorConfig
~~~~~~~~~~~~

For general editor settings related to indentation, maximum line length and line
endings, the settings in the ``.editorconfig`` file can be applied. This file can be
used in combination with a text editor or IDE that supports it. For more information,
take a look at the `EditorConfig <https://editorconfig.org>`__ documentation.

.. _development-tools-pre-commit:

pre-commit
~~~~~~~~~~

`pre-commit <https://pre-commit.com>`__ is a framework for managing and maintaining
multi-language pre-commit hooks, which get executed each time ``git commit`` is run. The
tool itself should be installed already. The hooks listed in ``.pre-commit-config.yaml``
can be installed by simply running:

.. code-block:: bash

    pre-commit install

The hooks can also be run manually on all versioned and indexed files using:

.. code-block:: bash

    pre-commit run -a

The versions of all hooks can be updated automatically by running:

.. code-block:: bash

    pre-commit autoupdate

black
~~~~~

`black <https://black.readthedocs.io>`__ is a code formatter which is used throughout
all Python code in the project. The tool itself should be installed already and can be
applied on one or multiple files using:

.. code-block:: bash

    black <path>

Besides running black on the command line, there are also various `integrations
<https://black.readthedocs.io/en/stable/integrations/editors.html>`__ available for
different text editors and IDEs. black is also part of the pre-commit hooks. As such, it
will run automatically on each commit or when running the pre-commit hooks manually.

isort
~~~~~

`isort <https://pycqa.github.io/isort/index.html>`__ is another kind of code formatter
with focus on sorting and grouping import statements throughout all Python code in the
project. The tool itself should be installed already and can be applied on one or
multiple files using:

.. code-block:: bash

    isort <path>

isort will automatically use the configuration specified in the ``[tool.isort]`` section
inside ``pyproject.toml``. Similar to black, various integrations are available for
different text editors and IDEs. Furthermore, isort is part of the pre-commit hooks and
will run automatically on each commit or when running the pre-commit hooks manually.

autoflake
~~~~~~~~~

`autoflake <https://github.com/PyCQA/autoflake>`__ is a tool mainly used to help with
the common case of removing unused import statements. The tool itself should be
installed already and can be applied on one or multiple files (via the ``-r`` flag)
using:

.. code-block:: bash

    autoflake -i -r <path>

autoflake will automatically use the configuration specified in the ``[tool.autoflake]``
section inside ``pyproject.toml``. As it also is part of the pre-commit hooks, there is
usually no need to run it manually. However, it may be necessary to exclude certain
files or imports, which can be achieved with one of the options shown in the following
example:

.. code-block:: python3

    # autoflake: skip_file

    import unused_import  # noqa

Pylint
~~~~~~

`Pylint <https://pylint.pycqa.org>`__ is a static code analysis tool for Python and
should already be installed as well. It can be used on the command line to aid with
detecting some common programming or style mistakes, even if not using an IDE that
already does that. For example, linting the whole ``kadi`` package can be done by
running the following command:

.. code-block:: bash

    pylint kadi

Pylint will automatically use the configuration specified in the ``[tool.pylint.*]``
sections inside ``pyproject.toml``. Sometimes, there might be certain code that should
never be checked for various things. Using specific comments, one can instruct Pylint to
skip such code, e.g. the following line will not raise a message for an unused import
statement:

.. code-block:: python3

    import something  # pylint: disable=unused-import

ESLint
~~~~~~

`ESLint <https://eslint.org>`__ is a linter and basic code formatter which is used for
all JavaScript code throughout the project, including any code snippets inside script
tags and Vue.js components. It should be already installed and can be applied on the
whole ``kadi`` folder using the ``eslint`` script exposed by the ``npm`` command. Note
that npm needs access to the ``package.json`` file, see also the section about
:ref:`managing frontend dependencies <development-coding-frontend-dependencies>`.

.. code-block:: bash

    npm run eslint kadi

ESLint will automatically use the configuration specified in ``eslint.config.cjs``.
Besides running ESlint on the command line, there are also various `integrations
<https://eslint.org/docs/user-guide/integrations>`__ available for different text
editors and IDEs. Some files also contain code that should never be checked for certain
things. Using specific comments again, one can instruct ESLint to skip such code, e.g.
the following will suppress errors for unused variables in the specified function:

.. code-block:: js

    /* eslint-disable no-unused-vars */
    function foo(a) {}
    /* eslint-enable no-unused-vars */

.. code-block:: js

    // eslint-disable-next-line no-unused-vars
    function foo(a) {}

ESLint is also part of the pre-commit hooks. As such, it will run automatically on each
commit or when running the pre-commit hooks manually.
