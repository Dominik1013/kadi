Welcome to |kadi|'s documentation!
==================================

|pypi| |license| |zenodo|

.. |pypi| image:: https://img.shields.io/pypi/v/kadi
    :target: https://pypi.org/project/kadi/
    :alt: PyPI

.. |license| image:: https://img.shields.io/pypi/l/kadi
    :target: https://opensource.org/licenses/Apache-2.0
    :alt: License

.. |zenodo| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.4088269.svg
    :target: https://doi.org/10.5281/zenodo.4088269
    :alt: Zenodo

**Kadi4Mat**, or **Kadi** for short, is a generic and open source virtual research
environment. Originally developed in the context of materials science, Kadi4Mat can be
used for the management of any type of research data within different research
disciplines and use cases.

----

This documentation is mainly focused on the administration and development of and with
|kadi|. It contains instructions on how to install, maintain and configure |kadi|, some
general topics about developing or contributing to |kadi|, information about the HTTP
API |kadi| provides as well as an overview and API reference of the source code itself.

For user-oriented information about |kadi|, including a public demo instance, please see
its `website <https://kadi.iam.kit.edu>`__. The source code of the project can be found
on `GitLab <https://gitlab.com/iam-cms/kadi>`__.

----

.. toctree::
    :name: installation
    :caption: Installation & Maintenance
    :maxdepth: 1

    installation/production/index
    installation/development/index
    installation/configuration/index
    installation/plugins

This chapter contains instructions on how to install, maintain and :ref:`configure
<installation-configuration>` |kadi| as well as its :ref:`plugins
<installation-plugins>`. For setting up |kadi| as a service in a production environment,
please refer to the :ref:`production <installation-production>` instructions. For
development installations, please refer to the :ref:`development
<installation-development>` instructions instead.

----

.. toctree::
    :name: development
    :caption: Development
    :maxdepth: 1

    development/overview
    development/tools
    development/coding
    development/testing
    development/documentation
    development/translations
    development/plugins
    development/issues

This chapter covers various information about developing or contributing to |kadi|.
Before reading this chapter, make sure you have a working :ref:`development
<installation-development>` environment.

----

.. toctree::
    :name: httpapi
    :caption: HTTP API
    :maxdepth: 1

    httpapi/intro
    httpapi/endpoints/index

This chapter covers the HTTP API provided by |kadi|, namely some :ref:`introductory
<httpapi-intro>` information as well as a list of all available API :ref:`endpoints
<httpapi-endpoints>`.

----

.. toctree::
    :name: apiref
    :caption: API reference
    :maxdepth: 1

    apiref/lib
    apiref/modules
    apiref/plugins

This chapter serves as an API reference for the source code of |kadi|, currently focused
on the backend code.

----

.. toctree::
    :name: release-history
    :caption: Release history
    :maxdepth: 2

    HISTORY.md
